<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\clientes;

class ClienteController extends Controller
{

    public function ListarCliente(Request $request)
    {
        $cliente = Cliente::all();
        return view(Cliente.Listar)->with('cliente', $cliente);
    } 

    public function AgregarCliente(Request $request)
    {
        $cliente = Cliente::all();
        return view(Cliente.Agregar)->with('cliente', $cliente);
    } 

    public function GuardarCliente(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'requerido',
            'apellidos' => 'requerido',
            'cedula' => 'requerido',
            'direccion' => 'requerido',
            'telefono' => 'requerido',
            'fecha_nacimiento' => 'requerido',
            'email' => 'requerido',
        ]);
        
        $producto = new Producto;
        $producto->nombre           = $request->nombre;
        $producto->apellido         = $request->apellido;
        $producto->cedula           = $request->cedula;
        $producto->direccion        = $request->direccion;
        $producto->telefono         = $request->telefono;
        $producto->fecha_nacimiento = $request->fecha_nacimiento;
        $producto->email            = $request->email;
        $producto->save();
        
        return redirect()->route('list.cliente');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
